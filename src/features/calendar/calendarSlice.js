import { createSlice } from "@reduxjs/toolkit";
import moment from "moment";
import { v4 as uuid } from "uuid";

const WEEKDAYS = 7;

const initialState = {
  calendarType: "Day",
  currentDate: "",
  displayedWeek: [],
  schedules: {
    "15/04/2022": [
      {
        id: "111",
        startTime: 11,
        endTime: 12,
        title: "오피스 아워",
        description: "at the park",
      },
      {
        id: "222",
        startTime: 14,
        endTime: 16,
        title: "콜라 사러 나가기",
        description: "in my room",
      },
    ],
    "16/04/2022": [
      {
        id: "444",
        startTime: 10,
        endTime: 20,
        title: "잠 자기",
        description: "like dead",
      },
      {
        id: "333",
        startTime: 5,
        endTime: 7,
        title: "방배김밥 가서 유부초밥 먹기",
        description: "like john lennon",
      },
    ],
  },
};

export const slice = createSlice({
  name: "calendar",
  initialState: initialState,
  reducers: {
    setCalendarType: (state, { payload: { type } }) => {
      state.calendarType = type;
    },
    setToday: (state, { payload }) => {
      const today = moment(payload.today);
      const week = [];

      for (let i = 0; i < WEEKDAYS; i++) {
        const date = today.startOf("week");

        week.push(date.add(i, "day").format("DD/MM/YYYY"));
      }

      state.currentDate = payload.today;
      state.displayedWeek = week;
    },
    addEvent: (
      state,
      { payload: { date, startTime, endTime, title, description } }
    ) => {
      if (state.schedules[date]) {
        state.schedules[date].push({
          id: uuid(),
          startTime,
          endTime,
          title,
          description,
        });
      } else {
        state.schedules[date] = [
          {
            id: uuid(),
            startTime,
            endTime,
            title,
            description,
          },
        ];
      }
    },
    removeEvent: (state, { payload: { date, id } }) => {
      if (!date || !id) return;

      if (state.schedules[date].length <= 1) {
        delete state.schedules[date];

        return;
      }

      const index = state.schedules[date].findIndex(
        (schedule) => schedule.id === id
      );

      state.schedules[date].splice(index, 1);
    },
  },
});

export const selectCalendar = (state) => state.calendar;

export const selectMatchingScheduleById =
  ({ scheduleId, date }) =>
  (state) => {
    if (!state.calendar.schedules[date]) return;

    return state.calendar.schedules[date].find(
      (schedule) => schedule.id === scheduleId
    );
  };

export const { setCalendarType, setToday, addEvent, removeEvent } =
  slice.actions;

export default slice.reducer;

export const getMatchingScheduleByTime = (
  calendar,
  startTime,
  endTime,
  date
) => {
  if (!calendar.schedules[date]) return;

  return calendar.schedules[date].find((schedule) => {
    const startsBeforeButEndsWithin = endTime > schedule.startTime;
    const startsWithin =
      startTime <= schedule.endTime &&
      startTime >= schedule.startTime &&
      endTime <= schedule.endTime;

    return startsBeforeButEndsWithin || startsWithin;
  });
};
