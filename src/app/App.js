import React, { useEffect } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { useDispatch } from "react-redux";

import Header from "../common/components/Header/Header";
import Calendar from "../common/components/Calendar";
import NewEventPage from "../common/components/Calendar/NewEvent/NewEventPage";
import Event from "../common/components/Calendar/Event.js/Event";
import { setToday } from "../features/calendar/calendarSlice";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    const today = new Date().toISOString();

    dispatch(setToday({ today }));
  }, [dispatch]);

  return (
    <div>
      <Header />
      <Routes>
        <Route path="/calendar" element={<Calendar />} />
        <Route path="/events/new" element={<NewEventPage />} />
        <Route path="/events/:eventId" element={<Event />} />
        <Route path="/" element={<Navigate replace to="/calendar" />} />
      </Routes>
    </div>
  );
}

export default App;
