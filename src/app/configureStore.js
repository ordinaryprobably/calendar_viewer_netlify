import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";

import calendarReducer from "../features/calendar/calendarSlice";

const store = configureStore({
  reducer: {
    calendar: calendarReducer,
  },
  middleware: [logger],
});

export default store;
