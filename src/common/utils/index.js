export const IncrementalArrayFromZero = {
  toBefore: (max) => {
    return Array.from(Array(max).keys());
  },
};
