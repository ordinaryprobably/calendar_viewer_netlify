const colors = {
  WARNING: "red",
  BLACK: "black",
  HOVER: "aliceblue",
  GREY: "#f7f7f7",
  BOOKED_EVENT: "#4285f4",
};

export default colors;
