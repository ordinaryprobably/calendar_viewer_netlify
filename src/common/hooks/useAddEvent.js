import { useState } from "react";
import { useDispatch } from "react-redux";

import {
  addEvent,
  getMatchingScheduleByTime,
} from "../../features/calendar/calendarSlice";

const useAddEvent = () => {
  const [error, setError] = useState("");

  const dispatch = useDispatch();

  const checkDuplicateAndBookEvent = (
    slice,
    { startTime, endTime, date, title, description }
  ) => {
    const bookedEvent = getMatchingScheduleByTime(slice, startTime, date);

    if (bookedEvent) {
      setError(() => "선택한 시간에 이미 예약된 이벤트가 있습니다.");

      return true;
    }

    dispatch(
      addEvent({
        date,
        startTime,
        endTime,
        title,
        description,
      })
    );
  };

  return { checkDuplicateAndBookEvent, error, setError };
};

export default useAddEvent;
