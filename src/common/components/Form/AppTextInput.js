import { useFormikContext } from "formik";
import React from "react";

import ErrorMessage from "./ErrorMessage";

function AppTextInput({ name, ...otherProps }) {
  const { setFieldValue, setFieldTouched, values, errors, touched } =
    useFormikContext();

  return (
    <>
      <input
        type="text"
        name={name}
        value={values[name]}
        onBlur={() => setFieldTouched(name)}
        onChange={(e) => setFieldValue(name, e.target.value)}
        {...otherProps}
      />
      {touched[name] && <ErrorMessage message={errors[name]} />}
    </>
  );
}

export default AppTextInput;
