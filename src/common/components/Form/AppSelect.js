import { useFormikContext } from "formik";
import React from "react";

import ErrorMessage from "./ErrorMessage";

function AppSelect({ name, options, placeholder }) {
  const { setFieldValue, errors, touched } = useFormikContext();

  return (
    <>
      <select name={name} onChange={(e) => setFieldValue(name, e.target.value)}>
        <option hidden>{placeholder}</option>
        {options}
      </select>
      {touched[name] && <ErrorMessage message={errors[name]} />}
    </>
  );
}

export default AppSelect;
