import React from "react";
import styled from "styled-components";

import colors from "../../utils/colors";

function ErrorMessage({ message }) {
  return <Message>{message}</Message>;
}

const Message = styled.span`
  color: ${colors.WARNING};
  font-size: 12px;
`;

export default ErrorMessage;
