import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import * as Yup from "yup";

import Modal from ".";
import { selectCalendar } from "../../../features/calendar/calendarSlice";
import AppForm from "../Form/AppForm";
import AppTextInput from "../Form/AppTextInput";
import ErrorMessage from "../Form/ErrorMessage";
import colors from "../../utils/colors";
import useAddEvent from "../../hooks/useAddEvent";

const CLOSE_MODAL = false;

export default function EventFormModal({
  information: { date, startTime },
  displayModal,
}) {
  const { checkDuplicateAndBookEvent, error } = useAddEvent();

  const calendar = useSelector(selectCalendar);

  const handleSubmit = ({ title, description }) => {
    const hasError = checkDuplicateAndBookEvent(calendar, {
      date: date,
      startTime,
      endTime: startTime + 1,
      title,
      description,
    });

    if (hasError) return;

    displayModal(false);
  };

  const handleDismiss = (condition) => (event) => {
    if (event.target.id === "background") displayModal(condition);
    if (event.target.id === "close-button") displayModal(condition);
  };

  return (
    <Modal>
      <Background onClick={handleDismiss(CLOSE_MODAL)} id="background">
        <Content>
          <button onClick={handleDismiss(CLOSE_MODAL)} id="close-button">
            CLOSE
          </button>
          <AppForm
            initialValues={{
              title: "",
              description: "",
            }}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <span>시작 시간: {startTime} 시</span>
            <span>종료 시간: {startTime + 1} 시</span>
            <AppTextInput
              name="title"
              placeholder="제목을 입력하세요."
              autoComplete="off"
            />
            <AppTextInput
              name="description"
              placeholder="설명을 입력하세요"
              autoComplete="off"
            />
            <button type="submit">이벤트 등록하기</button>
            {error && <ErrorMessage message={error} />}
          </AppForm>
        </Content>
      </Background>
    </Modal>
  );
}

const validationSchema = Yup.object().shape({
  title: Yup.string().min(1).max(40).required("이벤트 제목을 입력해 주세요."),
  description: Yup.string(),
});

const Background = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: fixed;
  padding: 20px;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  overflow: hidden;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.3);
  z-index: 2;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 10px;
  width: 300px;
  height: 300px;
  background-color: white;
  border: 1px solid ${colors.BLACK};

  form {
    display: flex;
    flex-direction: column;

    input {
      margin: 5px;
      padding: 5px;
    }
  }
`;
