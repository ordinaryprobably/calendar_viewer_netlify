import ReactDom from "react-dom";

export default function Modal({ children }) {
  const parentElement = document.querySelector("#modal");

  return ReactDom.createPortal(children, parentElement);
}
