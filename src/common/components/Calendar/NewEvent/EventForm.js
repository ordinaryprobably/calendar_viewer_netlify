import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";

import AppForm from "../../Form/AppForm";
import AppTextInput from "../../Form/AppTextInput";
import AppSelect from "../../Form/AppSelect";
import { IncrementalArrayFromZero } from "../../../utils";
import {
  addEvent,
  getMatchingScheduleByTime,
  removeEvent,
  selectCalendar,
} from "../../../../features/calendar/calendarSlice";
import ErrorMessage from "../../Form/ErrorMessage";

const START_TIME_OPTIONS = IncrementalArrayFromZero.toBefore(24).map((hour) => (
  <option value={hour} key={hour}>{`${hour}:00`}</option>
));

const END_TIME_OPTIONS = IncrementalArrayFromZero.toBefore(25).map(
  (hour) => 1 <= hour && <option value={hour} key={hour}>{`${hour}:00`}</option>
);

function EventForm({ selectedDate, setSelectedDate, editInformation }) {
  const [error, setError] = useState("");

  const calendar = useSelector(selectCalendar);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleSubmit = ({ title, description, startTime, endTime }, date) => {
    if (!selectedDate) return setError("날짜를 선택해 주세요.");

    const bookedEvent = getMatchingScheduleByTime(
      calendar,
      startTime,
      endTime,
      date
    );

    if (bookedEvent)
      return setError("선택한 시간에 이미 예약된 이벤트가 있습니다.");

    if (editInformation) {
      dispatch(
        removeEvent({ date: editInformation.date, id: editInformation.id })
      );
    }

    dispatch(
      addEvent({
        date,
        startTime: Number(startTime),
        endTime: Number(endTime),
        title,
        description,
      })
    );

    navigate("/calendar");
  };

  useEffect(() => {
    if (editInformation) {
      setSelectedDate(editInformation.date);
    }
  }, [setSelectedDate, editInformation]);

  return (
    <div>
      <h1>예약: {selectedDate || "날짜를 선택해 주세요."}</h1>
      <AppForm
        initialValues={{
          title: editInformation?.title || "",
          description: editInformation?.description || "",
          startTime: "",
          endTime: "",
        }}
        validationSchema={validationSchema}
        onSubmit={_.partial(handleSubmit, _, selectedDate)}
      >
        <FormWrap>
          <AppTextInput
            name="title"
            placeholder="제목을 입력하세요."
            autoComplete="off"
          />
          <AppTextInput
            name="description"
            placeholder="설명을 입력하세요."
            autoComplete="off"
          />
          <AppSelect
            name="startTime"
            placeholder="시작 시간"
            options={START_TIME_OPTIONS}
          />
          <AppSelect
            name="endTime"
            placeholder="종료 시간"
            options={END_TIME_OPTIONS}
          />
          {error && <ErrorMessage message={error} />}
          <button type="submit">{editInformation ? "Edit" : "Submit"}</button>
        </FormWrap>
      </AppForm>
    </div>
  );
}

const validationSchema = Yup.object().shape({
  title: Yup.string().min(1).max(40).required("이벤트 제목을 입력해 주세요."),
  description: Yup.string(),
  startTime: Yup.number().required("시작 시간을 입력해 주세요."),
  endTime: Yup.number()
    .required("종료 시간을 입력해 주세요.")
    .when(
      "startTime",
      (startTime, schema) =>
        startTime &&
        schema.min(
          startTime + 1,
          "시작 시간 보다 최소 한 시간 뒤에 종료해야 합니다."
        )
    ),
});

const FormWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;

  input,
  select {
    padding: 5px 5px;
    margin-bottom: 5px;
  }
`;

export default EventForm;
