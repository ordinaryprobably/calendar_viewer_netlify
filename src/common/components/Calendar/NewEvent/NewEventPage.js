import React, { useMemo, useState } from "react";
import { useSearchParams } from "react-router-dom";

import Monthly from "../Monthly/Monthly";
import EventForm from "./EventForm";

function NewEventPage() {
  const [selectedDate, setSelectedDate] = useState("");

  const [searchParams] = useSearchParams();

  const date = searchParams.get("date");
  const title = searchParams.get("title");
  const description = searchParams.get("description");
  const id = searchParams.get("id");

  const editInformation = useMemo(
    () => ({ date, title, description, id }),
    [date, title, description, id]
  );

  const isEditing = date && title && id;

  return (
    <div>
      <Monthly setSelectedDate={setSelectedDate} />
      <EventForm
        selectedDate={selectedDate}
        setSelectedDate={setSelectedDate}
        editInformation={isEditing ? editInformation : null}
      />
    </div>
  );
}

export default NewEventPage;
