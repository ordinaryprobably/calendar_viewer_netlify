import React, { useState } from "react";
import styled from "styled-components";
import { partial, first } from "lodash";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import Table from "../../../styled-components/Table/Table";
import EventCell from "../../../styled-components/Table/Cell";
import { selectCalendar } from "../../../../features/calendar/calendarSlice";
import EventFormModal from "../../Modal/EventFormModal";
import { IncrementalArrayFromZero } from "../../../utils/index.js";
import colors from "../../../utils/colors";

function Weekly() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedCell, setSelectedCell] = useState({
    date: "",
    startTime: "",
  });

  const { displayedWeek: currentWeek, schedules } = useSelector(selectCalendar);

  const navigate = useNavigate();

  const handleAddEvent = (day, hour) => {
    setIsModalVisible(true);
    setSelectedCell({
      date: day,
      startTime: hour,
    });
  };

  const navigateToEventPage = (schedule, date) => (e) => {
    e.stopPropagation();
    navigate(`/events/${schedule.id}?date=${date}`);
  };

  const insertEventsIntoCells = (hour) => {
    return currentWeek.map((day) => {
      if (schedules[day]) {
        const events = [];

        for (let schedule of schedules[day]) {
          if (schedule.startTime === hour) {
            events.push(
              <WeekEvent
                height={schedule.endTime - schedule.startTime}
                onClick={navigateToEventPage(schedule, day)}
                key={day}
              >
                {schedule.title}
              </WeekEvent>
            );
          }
        }

        return (
          <EventTd onClick={partial(handleAddEvent, day, hour)} key={day}>
            {events}
          </EventTd>
        );
      }

      return <td onClick={partial(handleAddEvent, day, hour)} key={day}></td>;
    });
  };

  return (
    <>
      <WeeklyTable>
        <thead>
          <tr>
            <td>Time</td>
            {currentWeek.map((day) => (
              <td key={day}>{first(day.split("/"))}</td>
            ))}
          </tr>
        </thead>
        <tbody>
          {IncrementalArrayFromZero.toBefore(24).map((hour) => (
            <tr key={hour}>
              <td>{`${hour}:00 - ${hour + 1}:00`}</td>
              {insertEventsIntoCells(hour)}
            </tr>
          ))}
        </tbody>
      </WeeklyTable>
      {isModalVisible && (
        <EventFormModal
          information={selectedCell}
          displayModal={setIsModalVisible}
        />
      )}
    </>
  );
}

const WeeklyTable = styled(Table)`
  tbody tr td {
    :not(:nth-child(1)) {
      width: calc(100% - 200px);

      :hover {
        background-color: ${colors.HOVER};
      }
    }
  }
`;

const WeekEvent = styled(EventCell)`
  top: 0;
`;

const EventTd = styled.td`
  position: relative;
`;

export default Weekly;
