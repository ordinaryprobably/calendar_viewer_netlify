import React from "react";
import moment from "moment";
import { partial } from "lodash";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import {
  selectCalendar,
  setToday,
} from "../../../../features/calendar/calendarSlice";
import colors from "../../../utils/colors";

const WEEKDAYS = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"].map(
  (day, i) => <th key={day + i}>{day}</th>
);

const NEXT_MONTH = 1;
const LAST_MONTH = 0;

function Monthly({ setSelectedDate }) {
  const { currentDate } = useSelector(selectCalendar);
  const dispatch = useDispatch();

  const getDaysBeforeFirstDayOfMonth = () => {
    const firstDayOfMonth = moment(currentDate).startOf("month").format("d");
    const daysBeforeFirstDay = [];

    for (let i = 0; i < firstDayOfMonth; i++) {
      daysBeforeFirstDay.push(<td key={i + "."}></td>);
    }

    return daysBeforeFirstDay;
  };

  const getDaysAfterFirstDayOfMonth = () => {
    const date = moment(currentDate);
    const daysAfterFirstDay = [];
    const today = moment().format("DD/MM/YYYY");

    for (let i = 0; i < date.daysInMonth(); i++) {
      const formattedDay = date
        .startOf("month")
        .add(i, "day")
        .format("DD/MM/YYYY");
      const day = formattedDay.slice(0, 2);

      daysAfterFirstDay.push(
        today === formattedDay ? (
          <td
            key={day}
            onClick={partial(setSelectedDate, formattedDay)}
            id="today"
          >
            {day}
          </td>
        ) : (
          <td key={day} onClick={partial(setSelectedDate, formattedDay)}>
            {day}
          </td>
        )
      );
    }

    return daysAfterFirstDay;
  };

  const generateCalendar = () => {
    const totalDays = [
      ...getDaysBeforeFirstDayOfMonth(),
      ...getDaysAfterFirstDayOfMonth(),
    ];
    const weeks = [];
    let days = [];

    totalDays.forEach((day, i) => {
      if (i % 7 !== 0) {
        days.push(day);
      } else {
        weeks.push(days);

        days = [day];
      }

      if (i === totalDays.length - 1) {
        weeks.push(days);
      }
    });

    return weeks.map((week, i) => <tr key={i + "_"}>{week}</tr>);
  };

  const calendar = generateCalendar();

  const changeMonth = (direction) => {
    switch (direction) {
      case LAST_MONTH:
        dispatch(
          setToday({
            today: moment(currentDate).subtract(1, "month").toISOString(),
          })
        );

        break;
      case NEXT_MONTH:
        dispatch(
          setToday({
            today: moment(currentDate).add(1, "month").toISOString(),
          })
        );

        break;
      default:
        return;
    }
  };

  const displayedDate = new Date(currentDate).toDateString();

  return (
    <>
      <div>
        <div>
          <button onClick={partial(changeMonth, LAST_MONTH)}>{"<-"}</button>
          <span>{displayedDate}</span>
          <button onClick={partial(changeMonth, NEXT_MONTH)}>{"->"}</button>
        </div>
      </div>
      <Table>
        <thead>
          <tr>{WEEKDAYS}</tr>
        </thead>
        <tbody>{calendar}</tbody>
      </Table>
    </>
  );
}

const Table = styled.table`
  border: 1px solid ${colors.BLACK};
  border-collapse: collapse;
  table-layout: fixed;
  width: 500px;
  margin: 20px 0;

  tr {
    text-align: center;
    border-bottom: 1px solid ${colors.BLACK};
  }

  td {
    border-right: 1px solid ${colors.BLACK};
    border-collapse: collapse;
  }

  tbody td {
    padding: 8px 5px;

    :hover {
      background-color: ${colors.HOVER};
    }
  }

  #today {
    color: blue;
    font-weight: 600;
  }
`;

export default Monthly;
