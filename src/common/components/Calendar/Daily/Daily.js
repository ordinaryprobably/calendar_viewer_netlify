import React, { useState } from "react";
import styled from "styled-components";
import { partial } from "lodash";
import moment from "moment";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import Table from "../../../styled-components/Table/Table";
import EventCell from "../../../styled-components/Table/Cell";
import { selectCalendar } from "../../../../features/calendar/calendarSlice";
import EventFormModal from "../../Modal/EventFormModal";
import { IncrementalArrayFromZero } from "../../../utils/index.js";
import colors from "../../../utils/colors";

function Daily() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedCell, setSelectedCell] = useState({
    date: "",
    startTime: "",
  });

  const { currentDate, schedules } = useSelector(selectCalendar);

  const navigate = useNavigate();

  const today = moment(currentDate);
  const formattedToday = today.format("DD/MM/YYYY");
  const MONTH_DATE = today.format("MMM Do");

  const todaySchedules = schedules[formattedToday] || [];

  const handleAddEvent = (hour) => {
    setIsModalVisible(true);
    setSelectedCell({
      date: formattedToday,
      startTime: hour,
    });
  };

  const navigateToEventPage = (schedule) => {
    navigate(`/events/${schedule.id}?date=${formattedToday}`);
  };

  return (
    <>
      <DailyTable>
        <thead>
          <tr>
            <td>Time</td>
            <td>{MONTH_DATE}</td>
          </tr>
        </thead>
        <tbody>
          {IncrementalArrayFromZero.toBefore(24).map((hour) => (
            <tr key={hour}>
              <td>{`${hour}:00 - ${hour + 1}:00`}</td>
              <EventTd
                onClick={partial(handleAddEvent, hour)}
                style={{ display: "block", position: "relative" }}
              >
                {todaySchedules.map(
                  (schedule) =>
                    schedule.startTime === hour && (
                      <EventCell
                        height={schedule.endTime - schedule.startTime}
                        onClick={partial(navigateToEventPage, schedule)}
                        key={schedule.id}
                      >
                        {schedule.title}
                      </EventCell>
                    )
                )}
              </EventTd>
            </tr>
          ))}
        </tbody>
      </DailyTable>
      {isModalVisible && (
        <EventFormModal
          information={selectedCell}
          displayModal={setIsModalVisible}
        />
      )}
    </>
  );
}

const DailyTable = styled(Table)`
  tbody tr td {
    :nth-child(2) {
      :hover {
        background-color: ${colors.HOVER};
      }
    }
  }
`;

const EventTd = styled.td`
  position: relative;
  display: block;
`;

export default Daily;
