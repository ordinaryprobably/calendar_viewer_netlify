import React from "react";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { partial } from "lodash";

import Daily from "./Daily/Daily";
import Weekly from "./Weekly/Weekly";
import {
  selectCalendar,
  setCalendarType,
  setToday,
} from "../../../features/calendar/calendarSlice";

const WEEK = "Week";
const DAY = "Day";
const FORWARD = "Forward";
const BACKWARD = "Backward";

function Calendar() {
  const dispatch = useDispatch();
  const { currentDate, calendarType } = useSelector(selectCalendar);

  const handleClick = (calendar, direction) => {
    if (calendar === WEEK) {
      if (direction === FORWARD) {
        dispatch(
          setToday({
            today: moment(currentDate).add(7, "day").toISOString(),
          })
        );
      } else {
        dispatch(
          setToday({
            today: moment(currentDate).subtract(7, "day").toISOString(),
          })
        );
      }
    } else {
      if (direction === FORWARD) {
        dispatch(
          setToday({
            today: moment(currentDate).add(1, "day").toISOString(),
          })
        );
      } else {
        dispatch(
          setToday({
            today: moment(currentDate).subtract(1, "day").toISOString(),
          })
        );
      }
    }
  };

  return (
    <>
      <div>
        <div>
          {moment(currentDate).format("YYYY MM DD")}
          <select
            value={calendarType}
            onChange={(event) =>
              dispatch(setCalendarType({ type: event.target.value }))
            }
          >
            <option value={WEEK}>{WEEK}</option>
            <option value={DAY}>{DAY}</option>
          </select>
          <button onClick={partial(handleClick, calendarType, BACKWARD)}>
            {"<-"}
          </button>
          <button onClick={partial(handleClick, calendarType, FORWARD)}>
            {"->"}
          </button>
        </div>
      </div>
      <div>
        {calendarType === WEEK && <Weekly />}
        {calendarType === DAY && <Daily />}
      </div>
    </>
  );
}

export default Calendar;
