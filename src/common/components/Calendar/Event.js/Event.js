import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import styled from "styled-components";

import {
  selectMatchingScheduleById,
  removeEvent,
} from "../../../../features/calendar/calendarSlice";

function Event() {
  const { eventId } = useParams();
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const eventDate = searchParams.get("date");

  const eventInfo = {
    scheduleId: eventId,
    date: eventDate,
  };

  const calendar = useSelector(selectMatchingScheduleById(eventInfo));
  const dispatch = useDispatch();

  const handleRemove = () => {
    dispatch(removeEvent({ date: eventDate, id: eventId }));
    navigate("/calendar");
  };

  const handleEventEdit = () => {
    navigate({
      pathname: "/events/new",
      search: `date=${eventDate}&title=${calendar.title}&description=${calendar.description}&id=${eventId}`,
    });
  };

  if (!calendar) return <h1>Invalid approach</h1>;

  return (
    <EventWrap>
      <h1>{calendar.title}</h1>
      <h4>{calendar.description}</h4>
      <span>시작 시간: {calendar.startTime} 시</span>
      <span>종료 시간: {calendar.endTime} 시</span>
      <button onClick={handleEventEdit}>이벤트 수정하기</button>
      <button onClick={handleRemove}>이벤트 삭제하기</button>
    </EventWrap>
  );
}

const EventWrap = styled.div`
  display: flex;
  flex-direction: column;
  width: 500px;

  button {
    padding: 5px;
    width: 300px;
    margin-top: 10px;
  }
`;

export default Event;
