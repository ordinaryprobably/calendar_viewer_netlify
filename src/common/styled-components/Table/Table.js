import styled from "styled-components";

import colors from "../../utils/colors";

const Table = styled.table`
  border: 1 solid black;
  border-collapse: collapse;
  table-layout: fixed;
  width: 100%;
  background-color: white;

  tr td {
    :nth-child(1) {
      width: 100px;
    }

    height: 40px;
    border-bottom: 1px solid ${colors.GREY};
    border-right: 1px solid ${colors.GREY};
  }
`;

export default Table;
