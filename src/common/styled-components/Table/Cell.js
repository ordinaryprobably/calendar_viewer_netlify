import styled from "styled-components";

import colors from "../../utils/colors";

const EventCell = styled.div`
  position: absolute;
  height: ${(props) => props.height * 40 + props.height * 3 - 2}px;
  background-color: ${colors.BOOKED_EVENT};
  border-radius: 3px;
  z-index: 1;
  width: 90%;
  color: white;
  display: flex;
  align-items: center;
  padding-left: 10px;
  cursor: pointer;
`;

export default EventCell;
